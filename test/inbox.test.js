const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const web3 = new Web3(ganache.provider());
const {abi, evm} = require('../compileInbox');
let accounts;
let inbox;
const initialMessage = 'Hi there!';
describe('Inbox contract', () => {
    beforeEach(async ()=> {
        //get a list of all account
        accounts = await web3.eth.getAccounts();
        //use one account to deploy contract
        inbox = await new web3.eth.Contract(abi)
            .deploy({
                data:evm.bytecode.object, 
                arguments: [initialMessage]
            })
            .send({from: accounts[0], gas:'1000000'});
    });
    
    it('deploys a contract',() => {
        assert.ok(inbox.options.address);
    });
    
    it('has a default message',async () => {
        const message = await inbox.methods.message().call();
        assert.equal(message,initialMessage);
    });
    
    it('can change the message', async () => {
        await inbox.methods.setMessage("bye").send({from:accounts[0]});
        const message = await inbox.methods.message().call();
        assert.equal(message,'bye');
    });
    
})
