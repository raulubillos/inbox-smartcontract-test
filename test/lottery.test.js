const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const web3 = new Web3(ganache.provider());
const {abi, evm} = require('../compileLottery');
let accounts;
let lottery;
describe('Lottery Contract', () => {
    beforeEach(async ()=> {
        //get a list of all account
        accounts = await web3.eth.getAccounts();
        //use one account to deploy contract
        lottery = await new web3.eth.Contract(abi)
            .deploy({
                data:evm.bytecode.object
            })
            .send({from: accounts[0], gas:'1000000'});
    });
    
    it('deploys a contract',() => {
        assert.ok(lottery.options.address);
    });

    it('allows one account to enter', async () => {
        await lottery.methods.enter().send({
            from: accounts[0],
            value: web3.utils.toWei('0.02','ether')
        });

        const players = await lottery.methods.getPlayers().call({
            from: accounts[0]
        });

        assert.equal(accounts[0], players[0]);
        assert.equal(1,players.length);
    });

    it('allows multiple account to enter', async () => {
        await lottery.methods.enter().send({
            from: accounts[0],
            value: web3.utils.toWei('0.02','ether')
        });

        await lottery.methods.enter().send({
            from: accounts[1],
            value: web3.utils.toWei('0.02','ether')
        });

        await lottery.methods.enter().send({
            from: accounts[2],
            value: web3.utils.toWei('0.02','ether')
        });

        const players = await lottery.methods.getPlayers().call({
            from: accounts[0]
        });

        assert.equal(accounts[0], players[0]);

        assert.equal(accounts[1], players[1]);
        assert.equal(accounts[2], players[2]);
        assert.equal(3,players.length);
    });

    it('manager is the one who depoyed the contract',async () => {
        assert.equal(await lottery.methods.manager().call(),accounts[0]);
    });    

    it('requires a minimun amount to enter', async () => {
        try{
            await lottery.methods.enter().send({
                from:accounts[1],
                value:0
            });
            assert(false);
        }catch(err){
            assert(err);
        }
    })

    it('only manager can call pickWiner', async () => {
        try{
            await lottery.methods.pickwinner().send({
                from:accounts[1]
            });
            assert(false);
        }catch(err){
            assert(err);
        }
    });

    it('sends money to the winner and resets the address', async () => {
        await lottery.methods.enter().send({
            from:accounts[0],
            value: web3.utils.toWei('2','ether')
        });

        const initialBalance = await web3.eth.getBalance(accounts[0]);

        await lottery.methods.pickWinner().send({
            from:accounts[0]
        });

        const finalBalance = await web3.eth.getBalance(accounts[0]);

        const difference = finalBalance - initialBalance;

        assert(difference > web3.utils.toWei('1.8', 'ether'))
    })
})

