const HDWalletProvider = require('@truffle/hdwallet-provider');
const Web3 = require('web3');
const { abi, evm } = require('./compileInbox');

const provider = new HDWalletProvider(
    'crumble bachelor raven laugh curve chef airport doll zero plastic bone anchor',
    "https://rinkeby.infura.io/v3/f5aa7effa9be47cb88a312a8045f5481"
);

const web3 = new Web3(provider);

let accounts;

let inbox;

async function deploy(){
    accounts = await web3.eth.getAccounts();
    console.log('Attempting to deploy from account', accounts[0]);
    inbox = await new web3.eth.Contract(abi)
        .deploy({data:evm.bytecode.object, arguments: ['Hi there']})
        .send({from: accounts[0], gas:'1000000'});
    console.log(inbox.options.address);
    provider.engine.stop();
}

deploy();