const path = require('path');
const fs = require('fs');
const solc = require('solc');

const inboxPath = path.resolve(__dirname,"contracts","inbox.sol");
const inboxSource = fs.readFileSync(inboxPath,"utf8");

const input =  {
    language: 'Solidity',
    sources:{
        'inbox':{
            content: inboxSource
        }
    },
    settings:{
        outputSelection: {
            '*': {
                '*': ['*']
            }
        }
    }
};

module.exports = JSON.parse(solc.compile(JSON.stringify(input))).contracts['inbox'].Inbox;

